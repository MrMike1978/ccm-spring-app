package com.statefarm.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcmSuperpetsApplication {

	public static void main(String[] args) {
		SpringApplication.run(CcmSuperpetsApplication.class, args);
	}

}
